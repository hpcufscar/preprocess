#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import sys


def convert_to_float(l_split: list, classes: list) -> list:
    for i in range(len(l_split)):
        if i not in classes:
            try:
                aux = float(l_split[i])
            except ValueError:
                aux = l_split[i]
            l_split[i] = aux
    return l_split


def write_to_file_ordered(d: dict) -> None:
    out_file = open("outputFromDict", "w")
    for k, v in d.items():
        for l in v[1]:
            out_file.write(','.join(map(str, l)))
    pass


def move_classes_to_the_end(classes: list, d: dict) -> dict:
    # receives the index of the class(es) and move that to the end of the instance
    for k in d.keys():
        for inst in d[k][1]:
            for i in range(len(classes)):
                inst.append(inst.pop(classes[i] - i))
    return d


def any_att_moved_before(index: int, moved: list) -> int:
    # checks if any attribute with index smaller than received index has been moved before
    # if yes, counts and returns this number
    a = 0
    for i in moved:
        if i < index:
            a += 1
    return a


def remove_attributes(ids: list, moved: list, d: dict) -> dict:
    # receives the index(es) of the attribute(s) to be removed and the list of attributes that have been moved
    for k in d.keys():
        for inst in d[k][1]:
            for i in range(len(ids)):
                # checks if there has been changes in attributes with smaller indexes and adjusts index accordingly
                attributes_moved = any_att_moved_before(ids[i], moved)
                inst.pop(ids[i] - i - attributes_moved)
    return d


def normalize_fields_0_to_1(fields: list, d: dict) -> dict:
    # in order to normalize, we'll have to iterate 2 times
    # FIRST for to check highest and lowest values for each field
    max_field = [0] * len(fields)
    min_field = [sys.maxsize] * len(fields)
    for k in d.keys():
        for l in d[k][1]:
            for i in range(len(fields)):
                if l[fields[i]] > max_field[i]:
                    max_field[i] = l[fields[i]]
                if l[fields[i]] < min_field[i]:
                    min_field[i] = l[fields[i]]
    # after this for we already have 2 lists containing the maximum and minimum values on the specified fields
    # time to update.. SECOND for
    for k in d.keys():
        for l in range(len(d[k][1])):
            for i in range(len(fields)):
                # normalize number and update on dictionary
                d[k][1][l][fields[i]] = (d[k][1][l][fields[i]] - min_field[i]) / (max_field[i] - min_field[i])
    return d


def binarize_nominal_fields_1_to_n(fields: list, d: dict) -> dict:
    # in order to normalize, we'll have to iterate 2 times
    # FIRST for to check the number of different classes in each attribute
    d_aux = {}
    for key in fields:
        d_aux[key] = []
    for k in d.keys():
        for l in d[k][1]:
            for i in range(len(fields)):
                if l[fields[i]].strip("\n") not in d_aux[fields[i]]:
                    d_aux[fields[i]].append(l[fields[i]].strip("\n"))
    # print(d_aux)
    # after this for we already have 1 dictionary containing all the nominal values for each specified field
    # time to create new attributes.. SECOND for
    # {1: ['http', 'dns', 'ssh', 'other', 'rdp', 'smtp', 'snmp', 'sip'], 23: ['tcp', 'udp', 'icmp']}
    for k in d.keys():
        for l in d[k][1]:
            for i in range(len(fields)):
                att = fields[i]
                # fields[i] is the key to new dict
                for v in d_aux[att]:
                    if l[att].strip("\n") == v:
                        # add new field with 1
                        l.append('1')
                    else:
                        # add new field with 0
                        l.append('0')
    return d


def main(file, classes, threshold, normalize, binarize, remove):
    d_combined = {}
    in_file = open(file, "r")
    for l in in_file.readlines():
        l_split = l.split("\t")
        # end program if user wants to move/normalize/binarize or the class is an index out of range
        if max(sorted(remove + classes + binarize + normalize)) > len(l_split):
            sys.exit("One of the indexes passed as argument is bigger than the number of fields.")
        # if it's a known IDS attack OR normal traffic
        if (l_split[14] != "0" and l_split[17] == "-1") or (l_split[17] == "1"):
            ids_split = l_split[14].split(",")
            l_split = convert_to_float(l_split, classes)
            # ids_detection will be used to compound the key for dict
            ids_detection = ""
            # if has IDS detection code, take out the ( ) for each code and generate the key
            if ids_split[0] != "0":
                for i in ids_split:
                    ids_detection += i[:-3]
                    # if it's not the last element, add semi colon
                    if ids_split.index(i) != (len(ids_split) - 1):
                        ids_detection += ","
                # outside the for, ids_detection is the first element of the combined key for the dict
                l_split[14] = ids_detection
            # if it's normal traffic, generate the key with 0
            else:
                l_split[14] = ids_split[0]
            # generate key with ids_detection and flag attributes
            key = l_split[14], l_split[17]
            # now that we have the key ready, we should add/insert to dict
            if key in d_combined:
                d_combined[key][0] += 1
                d_combined[key][1].append(l_split)
            else:
                d_combined[key] = [1, [l_split]]
    in_file.close()
    # after reading the whole file and creating the dict to store things
    # it is time to remove keys that appeared in less than <wanted number> instances
    for k in list(d_combined.keys()):
        if d_combined[k][0] < threshold:
            # d_combined.pop(k, None)
            del d_combined[k]
    # print a quick summary of which keys will be present in the final file
    instances = 0
    print("(key), # of instances\n---------------------")
    for w in sorted(d_combined, key=d_combined.get, reverse=True):
        print(str(w) + ", " + str(d_combined[w][0]))
        instances += d_combined[w][0]
    print("Total instances " + str(instances) + "\n\n\n")
    # normalize numeric
    if normalize:
        d_combined = normalize_fields_0_to_1(normalize, d_combined)
    # binarize nominal, move classes to end and remove old nominal attributes
    if binarize:
        d_combined = binarize_nominal_fields_1_to_n(binarize, d_combined)
        d_combined = move_classes_to_the_end(classes, d_combined)
        d_combined = remove_attributes(binarize, classes, d_combined)
    # remove remaining attributes the user wanted
    if remove:
        d_combined = remove_attributes(remove, sorted(binarize + classes), d_combined)
    # write dictionary entries to a file
    write_to_file_ordered(d_combined)
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Pre process the Kyoto database. This script gets the unmodified data "
                                                 "and outputs a csv file where only IDS detection and normal traffic"
                                                 " are present. Additionally, it's possible to remove flags under a "
                                                 "certain threshold of instances and normalize some attributes.")
    parser.add_argument("file", help="input file")
    requiredNamed = parser.add_argument_group('required named arguments')
    requiredNamed.add_argument("-c", "--classes", nargs='+', type=int, help='Index(es) of class(es) of the file',
                               required=True)
    parser.add_argument("-t", "--threshold", type=int, help="minimum number of instances to keep a key", default=1000)
    parser.add_argument("-n", "--normalize", nargs='+', type=int, help="normalize attributes from passed indexes "
                                                                       "from 0 to 1")
    parser.add_argument("-b", "--binarize", nargs='+', type=int, help="binarize attributes from passed indexes "
                                                                      "using 1-to-n")
    parser.add_argument("-r", "--remove", nargs='+', type=int, help="remove attributes from passed indexes")
    args = parser.parse_args()
    n, b, r, c = args.normalize, args.binarize, args.remove, args.classes
    if n is None:
        n = []
    if b is None:
        b = []
    if r is None:
        r = []
    # print(args.file, args.threshold, sorted(n), sorted(b), sorted(r))
    main(args.file, sorted(c), args.threshold, sorted(n), sorted(b), sorted(r))
    pass
